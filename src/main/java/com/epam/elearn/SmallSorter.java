package com.epam.elearn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sorter class that does all the work.
 */
public class SmallSorter {

    private final Logger logger = LoggerFactory.getLogger(SmallSorter.class);

    int numberOfArguments = 0;
    int[] argumentsArray = new int[0];

    /**
     * Method that sorts arguments in ascending order.
     * @param argumentsStringArray string array, containing arguments to be sorted
     */
    public boolean sortArgumentsArray(String[] argumentsStringArray) {

        try{
            validation(argumentsStringArray);
        } catch (NumberFormatException exception) {
            logger.error(exception.getMessage());
            return false;
        }

        for (int i = 0; i < numberOfArguments; i++) {
            for (int j = i + 1; j < numberOfArguments; j++) {
                if (argumentsArray[j] < argumentsArray[i]) {
                    int temp = argumentsArray[j];
                    argumentsArray[j] = argumentsArray[i];
                    argumentsArray[i] = temp;
                }
            }
        }

        logger.info("Arguments were sorted in ascending order.");

        return true;
    }

    /**
     * Method that prints out (and returns) string, containing sorters' arguments (presumably sorted, use after sortArgumentsArray)
     * @return string of arguments
     */
    public String getArgumentsArray(){

        StringBuilder builder = new StringBuilder("");
        for (int i = 0; i < numberOfArguments; i++) {
            builder.append(argumentsArray[i]).append(" ");
        }
        System.out.println(builder.toString());

        logger.info("Arguments array was printed out.");

        return builder.toString();
    }

    /**
     * Method that checks if entered arguments are valid (1-10 Integer arguments)
     * @param argumentsStringArray string array, containing arguments to be checked
     */
    private void validation(String[] argumentsStringArray){

        numberOfArguments = argumentsStringArray.length;
        argumentsArray = new int[numberOfArguments];

        if (numberOfArguments == 0) {

            throw new NumberFormatException("Number of arguments can't be 0!");

        }
        if (numberOfArguments > 10)  {

            throw new NumberFormatException("Number of arguments can't be more than 10!");

        }
        try{
            for (int i = 0; i < numberOfArguments; i++) {
                argumentsArray[i] = Integer.parseInt(argumentsStringArray[i]);
            }
        } catch (Exception e){

            throw new NumberFormatException("One if the arguments is not an integer number!");

        }

        logger.info("Arguments passed validation.");

    }

}
