package com.epam.elearn;

/**
 * Main class
 */
public class SortingApp
{
    /**
     * Main method, that starts the application. Sorts the command-line arguments and prints them out.
     * @param args string array, containing arguments to be sorted
     */
    public static void main( String[] args)
    {
        String[] str = {"2"};
        SmallSorter sorter = new SmallSorter();
        boolean noProblemSorting = sorter.sortArgumentsArray(str);
        if (noProblemSorting) sorter.getArgumentsArray();

    }
}
