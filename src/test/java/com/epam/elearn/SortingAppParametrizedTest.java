package com.epam.elearn;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;
import java.util.Arrays;
import java.util.Collection;

/**
 * Parametrized unit tests for Sorting App. Check what happens when valid arguments are entered.
 */
@RunWith(Parameterized.class)
public class SortingAppParametrizedTest {

    @Parameter(0)
    public String argumentsString;
    @Parameter(1)
    public String sortedArgumentsString;


    @Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {"5", "5 "},
                {"4 3 8 6", "3 4 6 8 "},
                {"0 9 8 7 6 5 4 3 2 1", "0 1 2 3 4 5 6 7 8 9 "},
                {"5 -5 4 -4 3 -3", "-5 -4 -3 3 4 5 "}});
    }

    @Test
    public void parametrizedTest()
    {
        String[] argumentsArray = argumentsString.split(" ");
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
        Assert.assertEquals(sortedArgumentsString, sorter.getArgumentsArray());
    }

}
