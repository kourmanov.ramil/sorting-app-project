package com.epam.elearn;

import org.junit.Test;

/**
 * Unit tests for Sorting App. Check what happens when invalid arguments are entered.
 */
public class SortingAppTest
{

    /**
     * Test if no arguments were entered
     */
    @Test(expected = NumberFormatException.class)
    public void emptyArgumentTest()
    {
        String[] argumentsArray = {""};
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
    }

    /**
     * Test if null argument was entered
     */
    @Test(expected = NumberFormatException.class)
    public void nullArgumentTest()
    {
        String[] argumentsArray = {null};
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
    }

    /**
     * Test if character was entered
     */
    @Test(expected = NumberFormatException.class)
    public void nonIntegerArgumentTest()
    {
        String[] argumentsArray = {"h"};
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
    }

    /**
     * Test if number more than maximum integer number was entered
     */
    @Test(expected = NumberFormatException.class)
    public void positiveOfRangeArgumentTest()
    {
        String[] argumentsArray = {"2147483648"};
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
    }

    /**
     * Test if number less than minimum integer number was entered
     */
    @Test(expected = NumberFormatException.class)
    public void negativeOfRangeArgumentTest()
    {
        String[] argumentsArray = {"-2147483649"};
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
    }

    /**
     * Test if some entered arguments were invalid
     */
    @Test(expected = NumberFormatException.class)
    public void mixArgumentsTest()
    {
        String[] argumentsArray = {"5", "4", "pr", "2147483648", "s"};
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
    }

    /**
     * Test if more than 10 (maximum allowed number of arguments) integer numbers were entered
     */
    @Test(expected = NumberFormatException.class)
    public void moreThanTenArgumentsTest()
    {
        String[] argumentsArray = {"11", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1"};
        SmallSorter sorter = new SmallSorter();
        sorter.sortArgumentsArray(argumentsArray);
    }

}
